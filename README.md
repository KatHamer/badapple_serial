# BadApple_Serial

Plays [Bad Apple](https://www.youtube.com/watch?v=FtutLA63Cp8) (or any other video) on a microcontroller over serial

# Why?

¯\\\_(ツ)\_/¯

# Usage

- Compile `main.cpp` for your desired microcontroller, you can use any tool for this, PlatformIO works well (A `platformio.ini` is included)
- I used [this library](https://github.com/jprodgers/LoLshield), since I used a LolShield as my display matrix, you can easily modify `main.cpp` to support any kind of display, make sure you update the dimensions and frame size in `__main__.py` and `main.cpp`, if you compile using the LolShield library, you may need to supress warnings in your compiler or use an older standard of C++
- Flash the firmware onto your microcontroller, pretty much anything will work, but I used an Arduino Uno.
- Figure out what port your microcontroller is connected to (usually `/dev/ttyUSBx` or `/dev/ttyACMx`)
- Create a venv: `python3 -m venv venv`
- Activate the venv: `source venv/bin/activate`
- Install requirements: `pip install -r requirements.txt`
- Run the encoder: `python3 __main__.py <serial-port> <video-file-name>`

