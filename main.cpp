/*
BadApple_Decoder
Decodes video bytes over serial
By Katelyn Hamer
*/

#include <Arduino.h>
#include <Charliplexing.h>

const int DISPLAY_HEIGHT = 9;
const int DISPLAY_WIDTH = 14;
const int FRAME_SIZE = 126;
const char TERM_CHAR = '_';

byte buffer[126];

void setup()
{
  Serial.begin(115200);
  LedSign::Init(); //Initializes the screen
  LedSign::Clear();
  pinMode(13, OUTPUT);
  digitalWrite(13, 0);
}

void loop()
{
  Serial.readBytesUntil(TERM_CHAR, buffer, FRAME_SIZE);  // Read the 126 byte frame data into the framebuffer
  for (int y=0; y < DISPLAY_HEIGHT; y++) {  // Iterate through the rows
    for (int x=0; x < DISPLAY_WIDTH; x++) {  // Iterate through the columns
      LedSign::Set(x, y, buffer[x + y * DISPLAY_WIDTH ]);  // Set pixel on the display at our current position to whatever is stored in the framebuffer
    }
  }
}