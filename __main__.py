#!/usr/bin/env python3
"""
BadApple_Serial
Converts a video to bytes that are sent over serial
By Katelyn Hamer
"""

# Note: This file is messy and hacky in places but I regret absolutely nothing

import serial
from PIL import Image
import numpy
import logging
import ffmpeg
import os
from pathlib import Path
from typing import Tuple
import sys
import time

DISPLAY_DIMENSIONS = (9, 14)  # Dimensions of desired display device
BAUD_RATE = 115200  # Baud rate of serial connection
FRAME_DIRECTORY = Path("frames")  # Path where frames are stored (WIPED AT RUNTIME)
BRIGHTNESS_THRESHOLD = (
    100  # Pixels with a brightness below this value are considered dark / 0
)
FRAME_DELAY = 0.2  # Delay between sending frames
TERMINATOR = "_"

# Validate command line arguments
if not len(sys.argv) == 3:
    print(f"Usage: {sys.argv[0]} <serial-device> <file-name>")
    exit(1)
else:
    SERIAL_DEVICE = sys.argv[1]
    VIDEO_FILE = Path(sys.argv[2])
    if not Path(SERIAL_DEVICE).exists():
        print(f"Supplied serial device '{SERIAL_DEVICE}' does not exist!")
        exit(2)
    if not VIDEO_FILE.exists():
        print(f"Supplied video file '{VIDEO_FILE.resolve()}' does not exist!")
        exit(3)

logging.basicConfig(level=logging.DEBUG)


def cleanup_frames_directory():  # TODO Validate arguments

    """Create frames directory if it doesn't exist, remove existing frames"""
    if FRAME_DIRECTORY.exists():
        logging.debug(
            f"Frame directory {FRAME_DIRECTORY.resolve()} exists, removing existing frames..."
        )
        for frame_file in FRAME_DIRECTORY.iterdir():
            logging.debug(f"Removing frame {frame_file}...")
            os.remove(frame_file)
    else:
        logging.debug("Frame directory doesn't exist, creating it...")
        FRAME_DIRECTORY.mkdir()


def split_into_frames(video_file: str):
    desired_width, desired_height = DISPLAY_DIMENSIONS
    (
        ffmpeg.input(VIDEO_FILE)
        .filter("fps", fps=5)
        .output(
            f"{FRAME_DIRECTORY.resolve()}/%d.png",
            video_bitrate="5000k",
            s=f"{desired_height}x{desired_width}",
            sws_flags="bilinear",
            start_number=0,
        )
        .run(capture_stdout=True, capture_stderr=True)
    )


def frame_to_bytes(frame_path: Path, dimensions: Tuple):
    """
    Convert a single frame to a list of binary values
    Whether pixels are assigned a 1 or 0 is dependent on brightness
    """
    frame_image = Image.open(frame_path)
    frame_np_array = numpy.array(frame_image)

    is_bright = lambda pixel: bool(
        sum(pixel) / 3 > BRIGHTNESS_THRESHOLD
    )  # Average RGB values and decide whether they are bright or not based on brightness threshold

    bitmap = [[is_bright(pixel) for pixel in row] for row in frame_np_array]
    logging.debug(f"{bitmap=}")
    flat_bitmap = [item for sublist in bitmap for item in sublist]
    flat_bitmap.append(ord(TERMINATOR))

    return bytearray(flat_bitmap)  # .reshape(dimensions)


def main():
    # Cleanup the frames directory and split the desired video into induvidua
    # l pngs
    cleanup_frames_directory()
    logging.debug(f"Splitting {VIDEO_FILE} into frames...")
    split_into_frames(VIDEO_FILE)

    # Ordered list of frame files
    logging.debug("Sorting frames...")
    sorted_frame_files = sorted(
        FRAME_DIRECTORY.iterdir(), key=lambda f: f.name.zfill(8)
    )

    # Convert each frame file into a list of bits that we can send over serial
    logging.debug("Converting frame files to bytes...")
    frame_bytes = [
        frame_to_bytes(frame_file, DISPLAY_DIMENSIONS)
        for frame_file in sorted_frame_files
    ]

    # Small delay to allow user to press play on an accomapnying media player
    logging.info("Press play now.")
    time.sleep(1)

    # Push the frames over serial
    logging.debug("Pushing bitmaps over serial...")
    with serial.Serial(SERIAL_DEVICE, BAUD_RATE) as ser:
        for index, frame in enumerate(frame_bytes):
            logging.debug(f"Sending frame #{index}/{len(frame_bytes)}...")
            bytes_written = ser.write(frame)
            logging.debug(f"Wrote {bytes_written} bytes")
            time.sleep(FRAME_DELAY)


if __name__ == "__main__":
    main()
